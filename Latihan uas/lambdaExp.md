# LAMBDA EXPRESSION
***Pengganti lambang lambda = &***

- T (True) = &xy.x
- F (False) = &xy.y

- ^ (Conjuntion) = &xy.xyF
- V (Disjunction) = &xy.xTy
- ~ (Negation) = &x.xFT

---

`Bukti T ^ F = F`
```
= ((&xy.xyF)T)F
= T F F
= ((&xy.x)F)F
= F
```

`Bukti T ^ T = T`
```
= ((&xy.xyF)T)T
= T T F
= ((&xy.x)T)F
= T
```

`Bukti F V T = T`
```
= ((&xy.xTy)F)T
= F T T
= ((&xy.y)T)T
= T
```

`Bukti T V T = T`
```
= ((&xy.xTy)T)T
= T T T
= ((&xy.x)T)T
= T
```

`Bukti F V F = F`
```
= ((&xy.xTy)F)F
= F T F
= ((&xy.y)T)F
= F
```

`Implementasi if-then-else`
```
if P then E1 else E2
= PE1E2

Bila P adalah True (T) maka
TE1E2
= (&xy.x)E1E2
= E1

Bila P adalah False (F) maka
FE1E2
= (&xy.y)E1E2
= E2
```