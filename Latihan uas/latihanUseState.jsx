import React from 'react';

const latihanUseState = () => {
    const name = useFormInput("Mary");
    const surname = useFormInput("Poppins");

    function useFormInput(initialValue) {
    const [value, setValue] = useState(initialValue);
    function handleChange(e) {
        setValue(e.target.value);
    }
    return {
            value,
            onChange: handleChange,
        };
    }
    return (
        <div>
            <input {...name} />
            <input {...surname} />
        </div>
    )
}

export default latihanUseState;
