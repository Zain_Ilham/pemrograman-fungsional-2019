lastList :: [a] -> a
lastList = foldl1 (\_ x -> x)

anotherLastList :: [a] -> a
anotherLastList = head . reverse

firstElem :: [a] -> a
firstElem = head

anotherFirstElem :: [a] -> a
anotherFirstElem = foldr1 (\_ x -> x)