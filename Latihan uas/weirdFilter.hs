notNumber x = x `notElem` "1234567890"
numberRemoval = filter notNumber

notChar x = x `notElem` "~`!@#$%^&*()_-+={[}]:;<,>.?/"
charRemoval = filter notChar

isVowel x = x `elem` "aiueoAIUEO"
countVowel = length . filter isVowel
isThreeOrLessVowels str = countVowel str < 4

isNotAppearTwiceInARow (a:b:ls) | (a==b) = False
isNotAppearTwiceInARow (a:ls)            = isNotAppearTwiceInARow ls
isNotAppearTwiceInARow []                = True

weirdFilter = filter isThreeOrLessVowels
            . filter isNotAppearTwiceInARow
            . map numberRemoval