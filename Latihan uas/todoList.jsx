import React, { useReducer, useEffect, useRef } from 'react'

const inputRef = useRef();

const initialState = [{id: 1, name: 'Get Started', complete: false}]

const todoReducer = (state, action) => {
    switch (action.type) {
        case 'ADD_TODO': {
            return (action.name.length)
                ? [...state, {
                    id: state.length ? Math.max(...state.map(todo => todo.id)) + 1 : 0,
                    name: action.name,
                    complete: false
                }]
                : state;
        }
    }
}

const [todos, dispatch] = useReducer(todoReducer, initialState)

function addTodo(event) {
    event.preventDefault();
    dispatch({
        type: 'ADD_TODO',
        name: inputRef.current.value,
        complete: false
    });

    inputRef.current.value = '';
}

<div className="todo-input">
    <form onSubmit="{addTodo}">
        <input ref="inputRef" type="search" id="add-todo" placeholder="Add Todo"/>
    </form>
</div>

export default todoList
