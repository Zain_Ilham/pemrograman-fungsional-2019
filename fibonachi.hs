-- first try
fibonacci = [a+b | a <- 1:fibonacci, b <- 0:1:fibonacci]

-- second try
fibonacci2 = zipWith (+) (1:fibonacci2) (0:1:fibonacci2)

-- slide
fibonacci3 = 1 : 1 : zipWith (+) fibonacci3 (tail fibonacci3)

fibonacci4 = fibo(1,1) where fibo(x,y) = x:fibo(y,x+y)

funLambda = \n -> \a -> \b -> n*(a-b)
