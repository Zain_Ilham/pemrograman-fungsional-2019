merge xs [] = xs
merge [] ys = ys
merge (x:xs) (y:ys) | x <= y = x:merge xs (y:ys)
                    | otherwise = y:merge (x:xs) ys

mergeSort [] = []
mergeSort [a] = [a]
mergeSort xs = merge (mergeSort (firstHalf xs)) (mergeSort (secondHalf xs))

firstHalf xs = let { n = length xs } in take (div n 2) xs
secondHalf xs = let { n = length xs } in drop (div n 2) xs

cobamerge [] = []
cobamerge [a] = [a]
cobamerge xs = merge (cobamerge (take(div (length xs) 2) xs)) (cobamerge (drop(div (length xs) 2) xs))