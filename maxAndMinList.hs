getMax x y
    | x <= y = y
    | otherwise = x

getMin x y
    | x <= y = x
    | otherwise = y

maxList (x:xs) = foldl getMax x xs

minList (x:xs) = foldr getMin x xs