fpb x y = head [z | z <- [(min x y), (min x y)-1 .. 1], x `mod` z == 0, y `mod` z == 0]
