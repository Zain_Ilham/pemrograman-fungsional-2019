plusThreeHighOrder xs = map (+3) xs
plusThreeComprehension xs = [x+3 | x <- xs]