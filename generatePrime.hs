prime = sieve [2..]
sieve (x:xs) = x : sieve [y | y <- xs , y `mod` x /= 0]