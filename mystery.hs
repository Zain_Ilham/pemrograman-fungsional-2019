mystery xs ys = concat (map (\x -> map (\y -> (x,y)) ys) xs)
mysteryComprehension xs ys = [(x,y) | x <- xs, y <- ys]