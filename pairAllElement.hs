{-
    Definisi instance Monad
    
-}

-- Comprehension Style
pairComprehension xs ys = [(x,y) | x <- xs, y <- ys ]

-- Monad Style Beautify
pairMonadBeauty xs ys = do
    x <- xs
    y <- ys
    return (x,y)

-- Monad Style Ugly
pairMonad xs ys = (xs >>= (\x -> ys >>= (\y -> return (x,y))))