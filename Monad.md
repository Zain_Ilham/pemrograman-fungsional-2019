# Apa itu Monad?
Kemampuan untuk mengatur data dan metadata (read:field)

## Example
```haskell
data Maybe a = Nothing
                | Just a

lookup :: a -> [(a, b)] -> Maybe b

animalFriends :: [ (String, String) ]
animalFriends = [ ("Pony", "Lion")
                , ("Lion", "Manticore")
                , ("Unicorn", "Lepricon") ]

```

*Bagaimana cara semua menemukan teman pony dan semua temannya teman pony?*

Kita harus membuat Maybe Monad

```haskell
-- 
-- (>>= :: m a -> ())
Just x >>= k = k x
Nothing >>= _ = Nothing

-- return :: a -> m a
return x = Just x
```

Fungsi TemanPony menjadi :
```haskell
monadicFriendLookup :: [ (String, String) ] -> Maybe String
monadicFriendLookup animalMap =
    lookup "Pony" animalMap
    >>= (\ponyFriend -> lookup ponyFriend animalMap
    >>= (\pony2ndFriend -> lookup pony2ndFriend animalMap
    >>= (\friend -> Just friend)))

-- other version
monadicFriendLookup :: [ (String, String) ] -> Maybe String
monadicFriendLookup animalMap = do
    ponyFriend <- lookup "Pony" animalMap
    ponyFriend1 <- lookup ponyFriend animalMap
    ponyFriend2 <- lookup ponyFriend1 animalMap
    return ponyFriend2
```

## Threading Program State

```haskell
type Sexpr = String

transformStmt :: Sexpr -> Int -> (Sexpr, Int)
transformStmt expr counter (newExpr, counter+1)
    where newExpr = "(define " ++ var ++ " " ++ expr ++ ")"
        var = "tmpvar" ++ (show counter) 
```