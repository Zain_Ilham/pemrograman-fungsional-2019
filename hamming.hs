-- Membuat barisan bilangan dimulai dari 1, masing - masing bilangan x akan 
-- ada kelipatan 2, 3, dan 5 di barisan tersebut.
-- xxs dan yys merupakan penulisan alias

merge xxs@(x:xs) yys@(y:ys)
    | x == y = x : merge xs ys
    | x < y = x : merge xs yys
    | x > y = y : merge xxs ys

hamming = 1 : merge (map (2*) hamming)
                    (merge (map (3*) hamming)
                            (map (5*) hamming))