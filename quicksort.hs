quickSort [] = []
quickSort (x:xs) = quickSort [y | y <- xs , y <= x]
                    ++ [x] ++
                    quickSort [z | z <- xs, z > x]