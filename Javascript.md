# Functional Programming : Javascript

## Pure Function
 - Mengembalikkan value berdasarkan argument yang diberikan kepadanya
 - Tidak mengubah argumennya atau variabel lain yang ada diluar fungsi
 - Mengembalikkan value yang sama setiap kali dipanggil menggunkan argumen yang sama

> Mengubah ubah argumen itu jelek, dapat menyebabkan:
> - Behavior program menjadi sulit di tebak
> - Susah (Mustahil) untuk melakukan test
> - Tidak thread-safe (tidak concurrent friendly)
> - Kurang modular, kode menjadi sulit dipindahkan atau reuse

## Referential Transparency
Sebuah fungsi dikatakan referentially transparent apabila fungsi tersebut diganti
langsung dengan body dari fungsi itu, maka akan tetap berjalan sesuai yang diinginkan

## High Order Function
Sebuah fungsi dikatakan apabila salah satu syarat dibawah ini terpenuhi:
- Menerima satu atau lebih fungsi sebagai argumennya
- Mengembalikkan sebuah fungsi sebagai hasilnya 

## Function Composition
Function composition adalah sebuah teknik untuk menggabungkan beberapa fungsi menjadi
fungsi yang lebih kompleks.

## Partial Application
Merupakan sebuah teknik pemrograman fungsi yang bergunaa untuk memberikan argumen yang
dibutuhkan fungsi pada waktu yang berbeda-beda

## Pipes
Merupakan sebuah fungsi atau operator yang digunakan untuk menjadikan output suatu fungsi
menjadi input fungsi yang lain.
