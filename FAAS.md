# Function as a Service
## Brief history of Cloud
 - Data centre : early history. Physical hosting environment. Hardware as the unit of scale
 - Infrastructure as a service : Menyiapkan layanan infrastruktur, sehingga orang lain
   hanya menyiapkan aplikasi saja. (AMAZON, ). OS as the unit of scale
 - Platform as a Service : Heroku. Application as the unit of scale.
 - serverless : Function as the unit of scale

## Benefit of Serverless
 - Reduce time to market
 - Lower operation costs

## Serverless Architecture
 - Terdapat Backend as a Service dan Function as a Service
 - dua duanya Mengurangi cost

### Backend as a sevice
Aplikasi yang hanya membutuhkan data basic seperti notifikasi atau authentication yang digunakan untuk aplikasi
simple seperti single-page web apps or mobile apps. Contoh **Firebase**

### Function as a Service
application where server-side logic is still writen by the application developer, but, unlike traditional ...
 - Automated elasticity
 - true pay per invokation
 - stateless
 - event based
 - choose memory -> computer oper, I/O
 - using container tech