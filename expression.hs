-- Suatu tipe data untuk mendeskripsikan expression
data Exp = C Float
          | V String
          | Exp :+ Exp
          | Exp :- Exp
          | Exp :* Exp
          | Exp :/ Exp
          | Let String Exp Exp
            deriving Show

subst :: String -> Exp -> Exp -> Exp
subst v0 e0 (V v1)         = if (v0 == v1) then e0 else (V v1)
subst v0 e0 (C c)          = (C c)
subst v0 e0 (e1 :+ e2)     = subst v0 e0 e1 :+ subst v0 e0 e2
subst v0 e0 (e1 :- e2)     = subst v0 e0 e1 :- subst v0 e0 e2
subst v0 e0 (e1 :* e2)     = subst v0 e0 e1 :* subst v0 e0 e2
subst v0 e0 (e1 :/ e2)     = subst v0 e0 e1 :/ subst v0 e0 e2
subst v0 e0 (Let v1 e1 e2) = Let v1 e1 (subst v0 e0 e2)

eval :: Exp -> Float
eval (C x) = x
eval (e1 :+ e2)    = eval e1 + eval e2
eval (e1 :- e2)    = eval e1 - eval e2
eval (e1 :* e2)    = eval e1 * eval e2
eval (e1 :/ e2)    = eval e1 / eval e2
eval (Let v e0 e1) = eval (subst v e0 e1)
eval (V _)         = 0.0

-- Implementasi eval untuk Fold

eval (add e1 e2)  = (eval e1) + (eval e2) 
eval (mult e1 e2) = (eval e1) * (eval e2)
eval (subt e1 e2) = (eval e1) - (eval e2)
eval (div e1 e2)  = (eval e1) / (eval e2)


-- Expression Fold 
foldExp (add, mult, subt, div) (C x) = x
foldExp (add, mult, subt, div) (x:+y) = add (foldExp (add, mult, subt, div) f x) (foldExp (add, mult, subt, div) f y)
foldExp (add, mult, subt, div) f (x:*y) = mult (foldExp (add, mult, subt, div) f x) (foldExp (add, mult, subt, div) f y)
foldExp (add, mult, subt, div) f (x:-y) = subt (foldExp (add, mult, subt, div) f x) (foldExp (add, mult, subt, div) f y)
foldExp (add, mult, subt, div) f (x:/y) = div (foldExp (add, mult, subt, div) f x) (foldExp (add, mult, subt, div) f y)

newEval = foldExp (add, mult, subt, div)
          where add = (+)
                mult = (*)
                subt = (-)
                div = (/)
